import * as types from '../actions/types';
import _ from 'lodash';

export default (state = {}, action) => {
    switch(action.type){
        case types.FETCH_STREAM:
            return {...state, [action.payload.id] : action.payload}
        case types.CREATE_STREAM:
            return {...state, [action.payload.id] : action.payload}
        case types.EDIT_STREAM: 
            return {...state, [action.payload.id] : action.payload}
        case types.DELETE_STREAM:
            return _.omit(state, action.payload)
        case types.FETCH_STREAMS:
            const arr = action.payload
            let newState = {...state}
            arr.forEach(rec => {
                newState = {...newState, [rec.id]: rec}
            });
            return newState;

            // return {...state, ..._.mapKeys(action.payload, 'id')} // this is the lodash solution
        default:
            return state;
    }
}