import React from 'react'
import Modal from '../Modal';
import history from '../../history';
import {connect} from 'react-redux';
import {fetchStream, deleteStream} from '../../actions';
import {Link} from 'react-router-dom';

class StreamDelete extends React.Component {

    componentDidMount() {
        if(!this.props.stream){
            this.props.fetchStream(this.props.match.params.id)
        }
    }
    
    renderActions() {
        const stream = this.props.stream;
        if(!stream) {
            return (
            <>
                <button className="ui button negative" disabled>Delete</button>
                <Link className="ui button" to={'/'}>Cancel</Link>
            </>    
            )    
        }else {
            return (
                <>
                    <button className="ui button negative" onClick={() => this.props.deleteStream(stream.id)}>Delete</button>
                    <Link className="ui button" to={'/'}>Cancel</Link>
                </>
            )
        }
    }
    /* 
    <React.Fragment></React.Fragment>
    */

    renderDescription = () => {
        if(!this.props.stream){
            return `Loading`
        }else{
            return `Are you sure you wanna delete the stream "${this.props.stream.title}" ?`
        }
    }

    render() {
        return (
            <Modal 
                title="Delete Stream"
                description={`${this.renderDescription()}`}
                actions={this.renderActions()} 
                onDismiss={() => history.push('/')}
            />
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        stream: state.streams[ownProps.match.params.id]
    }
}

export default connect(mapStateToProps,{fetchStream, deleteStream})(StreamDelete);