import React from 'react'
import {connect} from 'react-redux';
import {editStreams, fetchStream} from '../../actions/index';
import StreamForm from './StreamForm';

class StreamEdit extends React.Component {

    componentDidMount() {
        if(!this.props.stream){
            this.props.fetchStream(this.props.match.params.id);
        }
    }

    onSubmit = (formValues) => {
        this.props.editStreams(this.props.stream.id, formValues);
    }
    
    render() {
        if(!this.props.stream) {
            return(
                <div>
                    Loading..
                </div>
            )
        }
        return (
            <div>
                <h2>Edit your Stream</h2>
                <StreamForm 
                    initialValues={{title: this.props.stream.title, description:this.props.stream.description}}
                    onSubmit={this.onSubmit}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        stream: state.streams[ownProps.match.params.id]
    }
}

export default connect(mapStateToProps, {fetchStream,editStreams})(StreamEdit);