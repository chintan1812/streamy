import streams from '../api/streams'
import history from '../history'
import * as types from './types';

// App login

export const signIn = (userId) => {
    return {
        type: types.SIGN_IN,
        payload: userId
    }
}

export const signOut = () => {
    return {
        type: types.SIGN_OUT
    }
}

// Streams CRUD

export const createStream = (formValues) =>async (dispatch, getState) => {
    const {userId} = getState().auth
    const res = await streams.post('/streams', {...formValues, userId});
 
    dispatch({type: types.CREATE_STREAM, payload: res.data})
    // Do the navigation to the main screen once the stream creation is successful
    history.push('/')
}

export const fetchStreams = () => async (dispatch) => {
    const res = await streams.get('/streams');

    dispatch({type: types.FETCH_STREAMS, payload: res.data})
}

export const fetchStream = (id) => async (dispatch) => {
    const res = await streams.get(`/streams/${id}`);

    dispatch({type: types.FETCH_STREAM, payload: res.data});
}

export const editStreams = (id, formValues) => async (dispatch) => {
    const res = await streams.patch(`/streams/${id}`, formValues);

    dispatch({type: types.EDIT_STREAM, payload: res.data});
    history.push('/');
}

export const deleteStream = (id) => async (dispatch) => {
    await streams.delete(`/streams/${id}`);

    dispatch({type: types.DELETE_STREAM, payload: id})
    history.push('/');
}